package com.devsuperior.dsmovie.builders;

import com.devsuperior.dsmovie.entities.User;

public class UserBuilder {
	
	private Long id;
	private String email;

	public static UserBuilder anUser() {
		return new UserBuilder();
	}
	
	public UserBuilder withId(Long id) {
		this.id = id;
		return this;
	}
	
	public UserBuilder withemail(String email) {
		this.email = email;
		return this;
	}
	
	public User build() {
		return new User(this.id, this.email);
	}
}