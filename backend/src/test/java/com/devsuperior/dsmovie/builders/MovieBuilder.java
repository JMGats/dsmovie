package com.devsuperior.dsmovie.builders;

import com.devsuperior.dsmovie.entities.Movie;

public class MovieBuilder {
	
	private Long id;
	private String title;
	private Double score;
	private Integer count;
	private String image;
	
	public static MovieBuilder aMovie() {
		return new MovieBuilder();
	}
	
	public MovieBuilder withImage(String image) {
		this.image = image;
		return this;
	}
	
	public MovieBuilder withId(Long id) {
		this.id = id;
		return this;
	}

	public MovieBuilder withTitle(String title) {
		this.title = title;
		return this;
	}
	
	public MovieBuilder withScore(Double score) {
		this.score = score;
		return this;
	}
	
	public MovieBuilder withCount(Integer count) {
		this.count = count;
		return this;
	}
	
	public Movie build() {
		return new Movie(this.id, this.title, this.score, this.count, this.image);
	}
}
