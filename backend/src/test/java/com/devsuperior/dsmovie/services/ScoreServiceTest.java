package com.devsuperior.dsmovie.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.devsuperior.dsmovie.builders.MovieBuilder;
import com.devsuperior.dsmovie.dto.MovieDTO;
import com.devsuperior.dsmovie.dto.ScoreDTO;
import com.devsuperior.dsmovie.entities.Movie;
import com.devsuperior.dsmovie.entities.Score;
import com.devsuperior.dsmovie.entities.ScorePK;
import com.devsuperior.dsmovie.entities.User;
import com.devsuperior.dsmovie.repositories.MovieRepository;
import com.devsuperior.dsmovie.repositories.ScoreRepository;
import com.devsuperior.dsmovie.repositories.UserRepository;


@TestInstance(Lifecycle.PER_CLASS)
public class ScoreServiceTest {
	
	@Mock
	private MovieRepository movieRepository;
	
	@Mock
	private UserRepository userRepository;
	
	@Mock
	private ScoreRepository scoreRepository;
	
	@InjectMocks
	private ScoreService scoreService;
	
	@BeforeAll
	void setup() throws Exception {
		MockitoAnnotations.openMocks(this).close();
	}
	
	@Test
	public void saveScoreTest() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		String email = "email@email.com";
		Movie movie = MovieBuilder.aMovie().withId(3l).withCount(3).withTitle("titulo").withImage("image").withScore(2d).build();
		User user = new User(15l, email);
		ScorePK pk = new ScorePK();
		pk.setMovie(movie);
		pk.setUser(user);
		
		//criando set de scores para o filme
		Score score1 = new Score();
		score1.setMovie(movie);
		score1.setUser(user);
		score1.setValue(1d);

		Score score2 = new Score();
		score2.setMovie(movie);
		score2.setUser(user);
		score2.setValue(3d);
		
		Set<Score> scores = new HashSet<>();
		scores.add(score1);
		scores.add(score2);
		
		//captando e atribuindo o set ao atributo privado scores
		Field scoresAtt = Movie.class.getDeclaredField("scores");
		
		scoresAtt.setAccessible(true);
		scoresAtt.set(movie, scores);
		
		//preparando retorno de métodos
		Score scoreToReturn = new Score();
		score2.setMovie(movie);
		score2.setUser(user);
		score2.setValue(5d);
		
		Mockito.doReturn(user).when(userRepository).findByEmail(email);
		Mockito.doReturn(Optional.of(movie)).when(movieRepository).findById(3l);
		Mockito.doReturn(scoreToReturn).when(scoreRepository).saveAndFlush(Mockito.any());
		Mockito.doReturn(movie).when(movieRepository).save(movie);
		
		//execução do teste
		ScoreDTO dto = new ScoreDTO();
		dto.setEmail(email);
		dto.setMovieId(movie.getId());
		dto.setScore(5d);
		
		MovieDTO returned = scoreService.saveScore(dto);
		
		assertEquals(3d, returned.getScore());
	}
	
	

}
