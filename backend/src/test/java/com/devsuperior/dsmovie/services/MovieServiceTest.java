package com.devsuperior.dsmovie.services;

import static org.junit.jupiter.api.Assertions.assertAll;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.devsuperior.dsmovie.builders.MovieBuilder;
import com.devsuperior.dsmovie.dto.MovieDTO;
import com.devsuperior.dsmovie.entities.Movie;
import com.devsuperior.dsmovie.repositories.MovieRepository;

@TestInstance(Lifecycle.PER_CLASS)
public class MovieServiceTest {
	
	@Mock
	private MovieRepository repository;
	
	@InjectMocks
	private MovieService movieService;
	
	private Optional<Movie> toBeReturned;
	
	@BeforeAll
	void setup() throws Exception {
		MockitoAnnotations.openMocks(this).close();
		Movie tbr = MovieBuilder.aMovie().withId(1l).withTitle("titulo").withScore(5d).withCount(3).withImage("image").build();
		toBeReturned = Optional.of(tbr);
		Mockito.doReturn(toBeReturned).when(repository).findById(1l);
	}
	
	@Test
	public void findByIdTest() {
		MovieDTO movie = movieService.findById(1l);
		assertAll(() -> {movie.getId().equals(toBeReturned.get().getId());},
				  () -> {movie.getTitle().equals(toBeReturned.get().getTitle());},
				  () -> {movie.getCount().equals(toBeReturned.get().getCount());},
				  () -> {movie.getImage().equals(toBeReturned.get().getImage());});
	}
	
}
