package com.devsuperior.dsmovie.controllers;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.devsuperior.dsmovie.repositories.ScoreRepository;
import com.devsuperior.dsmovie.repositories.UserRepository;
import com.devsuperior.dsmovie.services.MovieService;
import com.devsuperior.dsmovie.services.ScoreService;

@WebMvcTest
@TestInstance(Lifecycle.PER_CLASS)
public class ScoreControllerTest {
	
	@Autowired
	private ScoreController scoreController;
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserRepository userRepository;
	
	@MockBean
	private ScoreRepository scoreRepository;
	
	@MockBean
	private MovieService movieService;
	
	@MockBean
	private ScoreService scoreService;

	@BeforeAll
	void setup() {
		standaloneSetup(this.scoreController);
	}
	
	@Test
	public void saveScoreTest() throws Exception {
		mockMvc.perform(
				put("/scores")
					.contentType(MediaType.APPLICATION_JSON)
						.content("{\"movieId\":1, \"email\":\"email@email.com\", \"score\":5.2}"))
							.andExpect(status().isOk());
	}
}
