package com.devsuperior.dsmovie.controllers;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.devsuperior.dsmovie.services.MovieService;
import com.devsuperior.dsmovie.services.ScoreService;

@WebMvcTest
public class MovieControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private MovieController movieController;
	
	@MockBean
	private MovieService movieService;
	
	@MockBean
	private ScoreService scoreService;
	
	@BeforeEach
	public void setup() {
		standaloneSetup(this.movieController);
	}

	@Test
	public void findByIdTest() throws Exception {
		mockMvc.perform(get("/movies/1")).andExpect(status().isOk());
	}

	@Test
	public void findAllTest() throws Exception {
		mockMvc.perform(get("/movies")).andExpect(status().isOk());
	}
	
}